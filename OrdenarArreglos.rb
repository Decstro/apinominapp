puts "Ordenar Arreglo"

n_elementos = 0
loop do
  puts "Ingrese el tamaño del arreglo:"
  n_elementos = gets.to_i
  break if n_elementos >= 10
  puts "El tamaño del arreglo debe ser mayor o igual a 10"
end

numeros = Array.new

def llenarArreglo(n, arreglo)
  for i in 0..n-1
  arreglo[i] = rand(0..30)
  end
end

def verificarNoRepetición(n, arreglo)
  loop do
    aux = Array.new
    for i in 0..n-1
      aux[i] = arreglo[i]
    end  

    cont = 0
    for i in 0..n-1
      for j in 0..n-1
        if aux[i] == arreglo[j]
          cont = cont + 1
        end   
      end
    end

    break if cont == n
    
    for i in 0..n-1
      arreglo[i] = rand(0..30)
    end
  end 
end

def mostrarArreglo(arreglo)
  puts "Array sin ordenar: #{arreglo}"
end

def definirRango(n, arreglo)
  index = 0
  while index < n - 1 do
    if arreglo[index] > arreglo[index+1]
      arreglo[index], arreglo[index+1] = arreglo[index+1], arreglo[index]
      index = 0
    else
      index += 1
    end
  end
  puts "Array ordenado: #{arreglo}"

  cont = 0
  range = Array.new
  aux = Array.new
  rangeSizes = Array.new
  for i in 0..n-1
    if arreglo[i+1] == arreglo[i] + 1
      if cont == 0
        aux.push(arreglo[i])
        aux.push(arreglo[i+1])
        cont += 2
      else 
        aux.push(arreglo[i+1])
        cont += 1
      end
    else
      if cont > 0
        rangeSizes.push(cont)
        cont = 0
      end   
    end
  end

  # Aqui hallamos el rango mas grande y la posición del rango 
  max = 0
  positionMax = 0
  for i in 0..rangeSizes.size-1
    if rangeSizes[i] > max
      max = rangeSizes[i]
      positionMax = i
    end
  end
  
  # Ahora hay que guardar los datos de ese rango
  if positionMax == 0
    inicio = 0
    for i in inicio..max-1
      range[i] = aux[i]
    end
  else
    inicio = 0
    for i in 0..positionMax-1
      inicio += rangeSizes[i]
    end
    for i in inicio..inicio+max-1
      range[i-inicio] = aux[i]
    end
  end
  
  # Solo hay que tomar el ultimo y el primer numero de ese rango
  
  puts "Array de rangos: #{aux}"
  puts "Rango seleccionado: #{range}"
  puts "Respuesta: [#{range.first},#{range.last}]"
end

# Escriba una función que tome una matriz de números enteros y devuelva una matriz 
# de longitud 2 que represente el rango más grande de números contenidos en esa matriz. 
# El primer número en la matriz de salida debe ser el primer número en el rango, mientras
# que el segundo número debe ser el último número en el rango. Un rango de números se define 
# como un conjunto de números que vienen uno detrás del otro en el conjunto de números enteros
# reales. Por ejemplo, la matriz de salida [2, 6] representa el rango {2, 3, 4, 5, 6},
# que es un rango de longitud 5. 
 
# Tenga en cuenta que no es necesario que los números estén ordenados o adyacentes en la matriz en 
# orden para formar un rango. Suponga que solo habrá un rango más grande.

# Entrada de muestra: [0, 7, 3, 13, 15, 5, 2, 4, 10, 7, 12, 6] Salida de muestra: [0, 7]
# [0, 1, 2, 3, 4, 5, 6, 7,  10, 12, 13, 15]
# Entrada de muestra: [1, 11, 3, 30, 12, 8, 2, 14, 10, 13] Salida de muestra: [11, 14]
# [1, 2, 3, 8, 10, 11, 12, 13, 14, 30]

#Inicia nuestro programa
llenarArreglo(n_elementos, numeros)
verificarNoRepetición(n_elementos, numeros)
mostrarArreglo(numeros)
definirRango(n_elementos, numeros)
