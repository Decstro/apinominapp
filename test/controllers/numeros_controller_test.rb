require "test_helper"

class NumerosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @numero = numeros(:one)
  end

  test "should get index" do
    get numeros_url, as: :json
    assert_response :success
  end

  test "should create numero" do
    assert_difference('Numero.count') do
      post numeros_url, params: { numero: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show numero" do
    get numero_url(@numero), as: :json
    assert_response :success
  end

  test "should update numero" do
    patch numero_url(@numero), params: { numero: {  } }, as: :json
    assert_response 200
  end

  test "should destroy numero" do
    assert_difference('Numero.count', -1) do
      delete numero_url(@numero), as: :json
    end

    assert_response 204
  end
end
